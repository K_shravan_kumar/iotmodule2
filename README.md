# Module 2 - Master the skills - Microprocessor Interfaces 

## What will you be learning in this Module?
You will be learning 
1. Basic Electronics concepts of basic electronics.  
2. Introduction to RPI
3. Basics of IoT Protocols
4. Shunya Interfaces API's

#### Completion and the Navigation of the Module is exactly same as the Orientation module, Please see this [link](https://gitlab.com/iotiotdotin/project-internship/orientation#how-to-complete-each-module-including-orientation-the-skilling-process) in-case you need a refresher on the Navigation of the module.